package eu.gaiax.keycloak;

import org.jboss.logging.Logger;
import org.keycloak.dom.saml.v2.assertion.AttributeStatementType;
import org.keycloak.models.*;
import org.keycloak.protocol.saml.mappers.AbstractSAMLProtocolMapper;
import org.keycloak.protocol.saml.mappers.AttributeStatementHelper;
import org.keycloak.protocol.saml.mappers.SAMLAttributeStatementMapper;
import org.keycloak.provider.ProviderConfigProperty;

import java.util.ArrayList;
import java.util.List;

public class GaiaXCustomProtocolMapper extends AbstractSAMLProtocolMapper implements SAMLAttributeStatementMapper {

    private static final Logger logger = Logger.getLogger(GaiaXCustomProtocolMapper.class);


    public static final String PROVIDER_ID = "gaiax-protocol-mapper";

    private static final List<ProviderConfigProperty> configProperties = new ArrayList<>();

    @Override
    public String getDisplayCategory() {
        return "Gaia-X FullName Attribute Mapper";
    }

    @Override
    public String getDisplayType() {
        return "Gaia-X FullName Mapper";
    }

    @Override
    public String getHelpText() {
        return "Concatenates firstname and lastname";
    }

    @Override
    public List<ProviderConfigProperty> getConfigProperties() {
        return configProperties;
    }

    @Override
    public String getId() {
        return PROVIDER_ID;
    }

    @Override
    public void transformAttributeStatement(AttributeStatementType attributeStatement, ProtocolMapperModel mappingModel, KeycloakSession session, UserSessionModel userSession, AuthenticatedClientSessionModel clientSession) {
        UserModel user = userSession.getUser();
        AttributeStatementHelper.addAttribute(attributeStatement, mappingModel, user.getFirstName() + " " + user.getLastName());
    }

    static {
        AttributeStatementHelper.setConfigProperties(configProperties);
    }

    public static ProtocolMapperModel createAttributeMapper(String name, String samlAttributeName, String nameFormat, String friendlyName) {
        logger.info("Create a protocol mapper Gaia-X Custom");
        return AttributeStatementHelper.createAttributeMapper(name, null, samlAttributeName, nameFormat, friendlyName, PROVIDER_ID);
    }


}
